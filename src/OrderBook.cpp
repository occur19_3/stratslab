#include "OrderBook.h"

#include "Transaction.h"

namespace Strats {

template<class Cmp>
JsonValue OrderBook::ToJsonArray(const std::map<OrderKey, std::unique_ptr<Order>, Cmp> &order_map) {
    JsonValue array = nlohmann::json::array();
    for (const auto &val: order_map) {
        array.push_back(val.second->GetJsonTree());
    }
    return array;
}

template<class Cmp>
void OrderBook::ClearFront(std::map<OrderKey, std::unique_ptr<Order>, Cmp> &order_map) {
    auto iter = order_map.begin();
    if (iter->second->IsFullfilled()) {
        order_map.erase(iter);
    }
}

JsonTree OrderBook::GetJsonTree() const {
    JsonTree r;
    r["buyOrders"] = ToJsonArray(buyOrders);
    r["sellOrders"] = ToJsonArray(sellOrders);
    return r;
}


std::vector<Transaction> OrderBook::AddOrder(OrderDirection direction, std::unique_ptr<Order> order) {
    switch (direction) {
    case OrderDirection::Buy:
        buyOrders.emplace(std::pair(order->GetPrice(), order->GetCreationTime()), std::move(order));
        break;

    case OrderDirection::Sell:
        sellOrders.emplace(std::pair(order->GetPrice(), order->GetCreationTime()), std::move(order));
        break;
    }
    return ProcessOrders();
}

std::vector<Transaction> OrderBook::ProcessOrders() {
    std::vector<Transaction> r;
    while (!buyOrders.empty() && !sellOrders.empty()) {
        std::optional<Transaction> tr = Order::MakeTransaction(*sellOrders.begin()->second, *buyOrders.begin()->second);
        if (!tr) {
            break;
        }
        ClearFront(buyOrders);
        ClearFront(sellOrders);
        r.push_back(*tr);
    }
    return r;
}

} // ::Strats
