#include "Transaction.h"

namespace Strats {

JsonTree Transaction::GetJsonTree() const {
    JsonTree r;
    STRATS_PP_PUT_VALUE(r, buyOrderId);
    STRATS_PP_PUT_VALUE(r, sellOrderId);
    STRATS_PP_PUT_VALUE(r, price);
    STRATS_PP_PUT_VALUE(r, quantity);
    return r;
}

} // ::Strats
