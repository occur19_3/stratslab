#include "Order.h"

#include <iostream>
#include <unordered_map>
#include "Transaction.h"


namespace Strats {

//Order

Order::Order(TimePoint creation_time, Id id, Price price, std::uint32_t quantity):
    creation_time(creation_time), id(id), price(price), quantity(quantity)
{}

JsonTree Order::GetJsonTree() const {
    JsonTree r;
    STRATS_PP_PUT_VALUE(r, id);
    STRATS_PP_PUT_VALUE(r, price);
    r["quantity"] = GetSingleTransactionQuantity();
    return r;
}

std::optional<Transaction> Order::MakeTransaction(Order &sell, Order &buy) {
    if (sell.price > buy.price) {
        return {};
    }
    std::uint32_t q = std::min(sell.GetSingleTransactionQuantity(), buy.GetSingleTransactionQuantity());
    sell.quantity -= q;
    buy.quantity -= q;
    return Transaction{buy.id, sell.id, buy.price, q};
}

STRATS_PP_ENUM_DEFINE_ENUM(STRATS_PP_ENUM_ORDER_TYPE)

STRATS_PP_ENUM_DEFINE_ENUM(STRATS_PP_ENUM_ORDER_DIRECTION)

} // ::Strats
