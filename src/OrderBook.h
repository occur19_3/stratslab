#pragma once

#include <map>
#include <memory>
#include <vector>

#include "utils/JsonTree.h"

#include "Order.h"

namespace Strats {

class Transaction;

class OrderBook {
public:
    JsonTree GetJsonTree() const;

    std::vector<Transaction> AddOrder(OrderDirection direction, std::unique_ptr<Order> order);

private:
    std::vector<Transaction> ProcessOrders();

    using OrderKey = std::pair<Order::Price, TimePoint>;
    template<class Cmp>
    struct OrderCmp {
        bool operator() (const OrderKey &o1, const OrderKey &o2) {
            if (o1.first == o2.first) {
                return o1.second < o2.second;
            }
            return Cmp()(o1.first, o2.first);
        }
    };

    using SellCmp = OrderCmp<std::less<void>>;
    using BuyCmp = OrderCmp<std::greater<void>>;

    template<class Cmp>
    static JsonValue ToJsonArray(const std::map<OrderKey, std::unique_ptr<Order>, Cmp> &order_map);

    template<class Cmp>
    static void ClearFront(std::map<OrderKey, std::unique_ptr<Order>, Cmp> &order_map);

    std::map<OrderKey, std::unique_ptr<Order>, BuyCmp> buyOrders;
    std::map<OrderKey, std::unique_ptr<Order>, SellCmp> sellOrders;
};

} // ::Strats
