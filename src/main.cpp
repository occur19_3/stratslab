#include <iostream>

#include "OrderBook.h"
#include "OrderParser.h"
#include "OrderProcess.h"

int main(int, char *[]) {
    Strats::OrderBook order_book;
    Strats::OrderParser parser;
    Strats::ProcessOrders(order_book, parser, std::cin, std::cout);
    return 0;
}
