#pragma once

#ifdef DEBUG

#include <boost/preprocessor/cat.hpp>

#include <iomanip>
#include <iostream>


// makro do zliczania ilosci argumentow makr
#define COUNT_PARMS2(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _, ...) _
#define COUNT_PARMS(...)\
  COUNT_PARMS2(__VA_ARGS__, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)


// wygodny skrot do wyswietlania zmiennych wewnatrz LOGow
// M wykorzystuje szablon do automatycznego wykorzystania getInfo()
#define LGS1(var) <<#var<<":"<<var<<", "
#define LGS2(var1,var2) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "
#define LGS3(var1,var2,var3) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "
#define LGS4(var1,var2,var3,var4) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "
#define LGS5(var1,var2,var3,var4,var5) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "
#define LGS6(var1,var2,var3,var4,var5,var6) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "
#define LGS7(var1,var2,var3,var4,var5,var6,var7) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "<<#var7<<":"<<var7<<", "
#define LGS8(var1,var2,var3,var4,var5,var6,var7,var8) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "<<#var7<<":"<<var7<<", "<<#var8<<":"<<var8<<", "
#define LGS9(var1,var2,var3,var4,var5,var6,var7,var8,var9) <<#var1<<":"<<var1<<", "<<#var2<<":"<<var2<<", "<<#var3<<":"<<var3<<", "<<#var4<<":"<<var4<<", "<<#var5<<":"<<var5<<", "<<#var6<<":"<<var6<<", "<<#var7<<":"<<var7<<", "<<#var8<<":"<<var8<<", "<<#var9<<":"<<var9<<", "

#define LGS(...) <<  " " BOOST_PP_CAT(LGS, COUNT_PARMS(__VA_ARGS__))(__VA_ARGS__)

#define PRINT_LOG(msg, funcname, filename, fileline) \
        do{ \
                std::cerr << " {" << std::setw(8) << ::Strats::StripFileName(filename) << ":" << std::setw(3) << fileline << "}" \
                        << "     " << msg << " {FUN=" << funcname << "}" << std::endl; \
        } while(0)


#define LOG(msg) PRINT_LOG(msg, __func__, __FILE__, __LINE__)

namespace Strats {

std::string StripFileName(const std::string& filename);

} ::Strats

#else

#define LOG(msg)

#endif  //DEBUG
