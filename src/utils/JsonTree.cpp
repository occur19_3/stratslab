#include "JsonTree.h"


namespace Strats {

JsonTree ReadJson(const std::string& str) {
    return nlohmann::json::parse(str);
}

} // ::Strats

//std::ostream & operator << (std::ostream &os, const Strats::JsonTree &json) {
////    rapidjson::OStreamWrapper osw(os);
////    rapidjson::Writer<rapidjson::OStreamWrapper> writer(osw);
////    json.Accept(writer);
//    os << json.dump();
//    os << std::endl;
//    return os;
//}
