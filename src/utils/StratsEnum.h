#pragma once

#include <string>

namespace Strats {
    template<class T>
    T from_string(const std::string &str);
}

#include "utils/EnumCreateMacro.h"
