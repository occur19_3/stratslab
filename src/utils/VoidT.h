#pragma once

namespace Strats {

template<class T>
using void_t = void;

} // ::Strats
