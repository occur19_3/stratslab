#pragma once

#include <type_traits>

#include "utils/VoidT.h"

namespace Strats {
namespace detail {

template<class, class = void>
struct has_GetJsonTree: std::false_type {};

template<class T>
struct has_GetJsonTree<T, void_t<decltype(std::declval<T>().GetJsonTree())>>: std::true_type {};

} // ::detail
} // ::Strats

template<class T>
typename std::enable_if<Strats::detail::has_GetJsonTree<T>::value, std::ostream &>::type
operator << (std::ostream &os, const T &v) {
    return os << v.GetJsonTree().dump() << "\n";
}
