#pragma once

#include <boost/preprocessor/seq.hpp>
#include <boost/preprocessor/stringize.hpp>

// declare_enum

#define STRATS_PP_ENUM_DECLARE_ENUM_CLASS(enum_name, enum_list) \
        enum class enum_name { BOOST_PP_SEQ_ENUM(enum_list) };

// to_string

#define STRATS_PP_ENUM_DECLARE_TO_STRING(enum_name) \
        std::string to_string(const enum_name);

#define STRATS_PP_ENUM_TO_STRING_CASE_FUNCTOR(r, enum_name, enum_value) \
        case enum_name::enum_value : return BOOST_PP_STRINGIZE(enum_value);

#define STRATS_PP_ENUM_DEFINE_TO_STRING(enum_name, enum_list)                                       \
        std::string to_string(const enum_name e) {                                                  \
            switch (e) {                                                                            \
                BOOST_PP_SEQ_FOR_EACH(STRATS_PP_ENUM_TO_STRING_CASE_FUNCTOR, enum_name, enum_list)  \
            default: return ""; }                                                                   \
        }

// from_string

#define STRATS_PP_ENUM_DECLARE_FROM_STRING(enum_name)  \
    template<> enum_name from_string<enum_name>(const std::string &str);

#define STRATS_PP_ENUM_FROM_STRING_FUNCTOR(r, enum_name, enum_value) \
        { BOOST_PP_STRINGIZE(enum_value) , enum_name :: enum_value },

#define STRATS_PP_ENUM_DEFINE_FROM_STRING(enum_name, enum_list)                                 \
        template<>                                                                              \
        enum_name from_string<enum_name>(const std::string &str) {                                     \
            static const std::unordered_map<std::string, enum_name> names {                      \
                BOOST_PP_SEQ_FOR_EACH(STRATS_PP_ENUM_FROM_STRING_FUNCTOR, enum_name, enum_list) \
            };                                                                                  \
            auto iter = names.find(str);                                                        \
            if (iter == names.end())  {                                                         \
                throw std::runtime_error(std::string("invalid enum name ") + #enum_name + " " + str);              \
            }                                                                                   \
            return iter->second;                                                                \
        }

/** trzeba uważać, żeby zadeklarować enuma w odpowiednim namespace */

//type
// declaration

#define STRATS_PP_ENUM_DECLARE_ENUM_IMPL(enum_name, enum_list)  \
        STRATS_PP_ENUM_DECLARE_ENUM_CLASS(enum_name, enum_list) \
        STRATS_PP_ENUM_DECLARE_TO_STRING(enum_name)             \
        STRATS_PP_ENUM_DECLARE_FROM_STRING(enum_name)

#define STRATS_PP_ENUM_DECLARE_ENUM(enum_tuple)                 \
        STRATS_PP_ENUM_DECLARE_ENUM_IMPL enum_tuple

// definition

#define STRATS_PP_ENUM_DEFINE_ENUM(enum_tuple)          \
        STRATS_PP_ENUM_DEFINE_TO_STRING enum_tuple      \
        STRATS_PP_ENUM_DEFINE_FROM_STRING enum_tuple
