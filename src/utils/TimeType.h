#pragma once

#include <chrono>

namespace Strats {

using Clock = std::chrono::steady_clock;
using TimePoint = Clock::time_point;
using TimeDuration = Clock::duration;

inline TimePoint now() {
    return std::chrono::steady_clock::now();
}

} // ::Strats
