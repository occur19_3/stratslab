#pragma once

#include <ostream>
#include "nlohmann/json.hpp"

namespace Strats {

using JsonTree = nlohmann::json;
using JsonValue = JsonTree;

JsonTree ReadJson(const std::string& str);

} // ::Strats

//std::ostream & operator << (std::ostream &os, const Strats::JsonTree &json);

#define STRATS_PP_PUT_VALUE(r, name) r[#name] = name


