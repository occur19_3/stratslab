#include "Logger.h"


#ifdef DEBUG

namespace Strats {

std::string StripFileName(const std::string& filename) {
    for (int i = filename.size(); i != -1; --i) {
        if (filename[i] == '/') {
            return filename.substr(i + 1);
        }
    }
    return filename;
}

} ::Strats

#endif
