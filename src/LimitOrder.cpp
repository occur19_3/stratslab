#include "LimitOrder.h"

namespace Strats {

LimitOrder::LimitOrder(TimePoint creation_time, Id id, Price price, std::uint32_t quantity):
    Order(creation_time, id, price, quantity)
{}

} // ::Strats
