#pragma once

#include <optional>

#include "utils/StratsEnum.h"
#include "utils/JsonTree.h"
#include "utils/TimeType.h"

namespace Strats {

class Transaction;

#define STRATS_PP_ENUM_ORDER_TYPE (OrderType, (Limit)(Iceberg))

STRATS_PP_ENUM_DECLARE_ENUM(STRATS_PP_ENUM_ORDER_TYPE)

#define STRATS_PP_ENUM_ORDER_DIRECTION (OrderDirection, (Buy)(Sell))

STRATS_PP_ENUM_DECLARE_ENUM(STRATS_PP_ENUM_ORDER_DIRECTION)

// Order

class Order {
public:
    using Id = std::uint32_t;
    using Price = std::uint32_t;

    Order(TimePoint creation_time, Id id, Price price, std::uint32_t quantity);
    virtual ~Order() = default;

    bool IsFullfilled() const { return quantity == 0u; }
    const TimePoint & GetCreationTime() const { return creation_time; }
    const Price & GetPrice() const { return price; }

    JsonTree GetJsonTree() const;

    static std::optional<Transaction> MakeTransaction(Order &sell, Order &buy);

protected:
    virtual std::uint32_t GetSingleTransactionQuantity() const = 0;

    const TimePoint creation_time;
    const Id id;
    const Price price;

    std::uint32_t quantity;
};

} // ::Strats

