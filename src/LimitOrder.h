#pragma once

#include "Order.h"

namespace Strats {

class LimitOrder: public Order {
public:
    LimitOrder(TimePoint creation_time, Id id, Price price, std::uint32_t quantity);

private:
    virtual std::uint32_t GetSingleTransactionQuantity() const { return quantity; }
};

} // ::Strats
