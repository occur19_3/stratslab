#pragma once

#include <iostream>

namespace Strats {

class OrderBook;
class OrderParser;

void ProcessOrders(OrderBook &book, OrderParser &parser, std::istream &is, std::ostream &os);

} // ::Strats

