#include "gtest/gtest.h"

#include "OrderBook.h"
#include "Transaction.h"
#include "LimitOrder.h"
#include "IcebergOrder.h"

namespace Strats {
namespace Test {

struct OrderBookTestData {
    std::vector<std::pair<OrderDirection, std::unique_ptr<Order>>> orders;
    std::vector<Transaction> transactions;
};

class OrderBookTest: public ::testing::TestWithParam<std::shared_ptr<OrderBookTestData>>
{
protected:
    void Run();

    std::vector<Transaction> transactions_;
    OrderBook order_book_;
};

void OrderBookTest::Run() {
   for (auto &order: GetParam()->orders) {
       auto trs = order_book_.AddOrder(order.first, std::move(order.second));
       transactions_.insert(transactions_.end(), trs.begin(), trs.end());
   }
   const auto & cmp_trans = GetParam()->transactions;

   bool tr_equal = std::equal(transactions_.begin(), transactions_.end(), cmp_trans.begin(), cmp_trans.end(),
              [] (const Transaction &t1, const Transaction &t2) {
        return std::tie(t1.buyOrderId, t1.sellOrderId, t1.price, t1.quantity) ==
                std::tie(t2.buyOrderId, t2.sellOrderId, t2.price, t2.quantity);
   });
   ASSERT_TRUE(tr_equal);
}

// TestCases

static std::vector<std::shared_ptr<OrderBookTestData>> MakeOrderTests() {
    std::vector<std::shared_ptr<OrderBookTestData>> r;
    TimePoint t1 = now();
    {
        std::vector<std::pair<OrderDirection, std::unique_ptr<Order>>> orders;
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Buy, std::make_unique<LimitOrder>(t1, 1, 12, 10))
        );
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Sell, std::make_unique<LimitOrder>(t1, 40, 10, 10))
        );

        std::vector<Transaction> transactions;
        transactions.push_back(Transaction{1, 40, 12, 10});
        r.push_back(std::shared_ptr<OrderBookTestData>(new OrderBookTestData{std::move(orders), std::move(transactions)}));
    }

    {
        std::vector<std::pair<OrderDirection, std::unique_ptr<Order>>> orders;
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Buy, std::make_unique<LimitOrder>(t1, 1, 10, 10))
        );
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Sell, std::make_unique<LimitOrder>(t1, 40, 12, 10))
        );

        std::vector<Transaction> transactions;
        r.push_back(std::shared_ptr<OrderBookTestData>(new OrderBookTestData{std::move(orders), std::move(transactions)}));
    }

    {
        std::vector<std::pair<OrderDirection, std::unique_ptr<Order>>> orders;
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Buy, std::make_unique<LimitOrder>(t1, 1, 10, 10))
        );
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Sell, std::make_unique<IcebergOrder>(t1, 40, 10, 10, 5))
        );

        std::vector<Transaction> transactions;
        transactions.push_back(Transaction{1, 40, 10, 5});
        transactions.push_back(Transaction{1, 40, 10, 5});
        r.push_back(std::shared_ptr<OrderBookTestData>(new OrderBookTestData{std::move(orders), std::move(transactions)}));
    }

    {
        std::vector<std::pair<OrderDirection, std::unique_ptr<Order>>> orders;
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Buy, std::make_unique<IcebergOrder>(t1, 1, 10, 12, 7))
        );
        orders.push_back(
            std::pair<OrderDirection, std::unique_ptr<Order>>(OrderDirection::Sell, std::make_unique<IcebergOrder>(t1, 40, 10, 12, 5))
        );

        std::vector<Transaction> transactions;
        transactions.push_back(Transaction{1, 40, 10, 5});
        transactions.push_back(Transaction{1, 40, 10, 5});
        transactions.push_back(Transaction{1, 40, 10, 2});
        r.push_back(std::shared_ptr<OrderBookTestData>(new OrderBookTestData{std::move(orders), std::move(transactions)}));
    }

    return r;
}

INSTANTIATE_TEST_CASE_P(Test, OrderBookTest, ::testing::ValuesIn(MakeOrderTests()));

TEST_P(OrderBookTest, SimpleTest) {

    Run();
}

} // ::Strats
} // ::Test
