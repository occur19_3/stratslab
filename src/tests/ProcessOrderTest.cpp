#include "gtest/gtest.h"

#include <deque>

#include "OrderBook.h"
#include "OrderParser.h"
#include "OrderProcess.h"
#include "Transaction.h"

namespace Strats {
namespace Test {

struct OrderProcessTestData {
    std::string in_data;
    std::deque<std::string> out_lines;
};

class OrderProcessTest: public ::testing::TestWithParam<OrderProcessTestData>
{
protected:
    void Run();

    OrderParser parser_;
    OrderBook order_book_;
};

void OrderProcessTest::Run() {
    std::stringstream in_ss, out_ss;
    in_ss << GetParam().in_data;
    ProcessOrders(order_book_, parser_, in_ss, out_ss);
    std::string out_line;
    size_t i = 0;
    while (std::getline(out_ss, out_line)) {
        ASSERT_LT(i, GetParam().out_lines.size());
        EXPECT_EQ(out_line, GetParam().out_lines[i]);
        ++i;
    }
}

static std::vector<OrderProcessTestData> MakeOrderProcessTests() {
    std::vector<OrderProcessTestData> r;
    std::string in_data;
    std::deque<std::string> out_lines;
    in_data += "{\"type\":\"Limit\",\"order\":{\"direction\":\"Buy\",\"id\":1,\"price\":14,\"quantity\":20}}\n";
    out_lines.push_back("{\"buyOrders\":[{\"id\":1,\"price\":14,\"quantity\":20}],\"sellOrders\":[]}");
    in_data += "{\"type\":\"Iceberg\",\"order\":{\"direction\":\"Buy\",\"id\":2,\"price\":15,\"quantity\":50,\"peak\":20}}\n";
    out_lines.push_back("{\"buyOrders\":[{\"id\":2,\"price\":15,\"quantity\":20},{\"id\":1,\"price\":14,\"quantity\":20}],\"sellOrders\":[]}");
    in_data += "{\"type\":\"Limit\",\"order\":{\"direction\":\"Sell\",\"id\":3,\"price\":16,\"quantity\":15}}\n";
    out_lines.push_back("{\"buyOrders\":[{\"id\":2,\"price\":15,\"quantity\":20},{\"id\":1,\"price\":14,\"quantity\":20}],\"sellOrders\":[{\"id\":3,\"price\":16,\"quantity\":15}]}");
    in_data += "{\"type\":\"Limit\",\"order\":{\"direction\":\"Sell\",\"id\":4,\"price\":13,\"quantity\":60}}\n";
    out_lines.push_back("{\"buyOrders\":[{\"id\":1,\"price\":14,\"quantity\":10}],\"sellOrders\":[{\"id\":3,\"price\":16,\"quantity\":15}]}");
    out_lines.push_back("{\"buyOrderId\":2,\"price\":15,\"quantity\":20,\"sellOrderId\":4}");
    out_lines.push_back("{\"buyOrderId\":2,\"price\":15,\"quantity\":20,\"sellOrderId\":4}");
    out_lines.push_back("{\"buyOrderId\":2,\"price\":15,\"quantity\":10,\"sellOrderId\":4}");
    out_lines.push_back("{\"buyOrderId\":1,\"price\":14,\"quantity\":10,\"sellOrderId\":4}");
    r.push_back(OrderProcessTestData{in_data, std::move(out_lines)});
    return r;
}

INSTANTIATE_TEST_CASE_P(Test, OrderProcessTest, ::testing::ValuesIn(MakeOrderProcessTests()));

TEST_P(OrderProcessTest, SimpleTest) {
    Run();
}

} // ::Strats
} // ::Test
