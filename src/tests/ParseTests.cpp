#include "gtest/gtest.h"

#include "LimitOrder.h"
#include "IcebergOrder.h"
#include "OrderParser.h"
#include "Transaction.h"

namespace Strats {
namespace Test {

struct ParseOrderData {
    std::string line;
    OrderDirection direction;
    OrderType type;
    std::shared_ptr<Order> order;
};

class ParseOrderTest: public ::testing::TestWithParam<ParseOrderData>
{
protected:
    void Run();
    OrderParser order_parser;
};

void ParseOrderTest::Run() {
    const ParseOrderData &d = GetParam();
    auto r = order_parser.ParseOrder(d.line);
    ASSERT_EQ(r.first, d.direction);
    ASSERT_NE(nullptr, r.second);
    ASSERT_EQ(r.second->GetPrice(), d.order->GetPrice());
    ASSERT_EQ(r.second->GetJsonTree(), d.order->GetJsonTree());

    switch (d.type) {
    case OrderType::Iceberg: {
        auto ptr = dynamic_cast<const IcebergOrder*>(r.second.get());
        ASSERT_NE(nullptr, ptr);
    }
        break;

    case OrderType::Limit: {
        auto ptr = dynamic_cast<const LimitOrder*>(r.second.get());
        ASSERT_NE(nullptr, ptr);
    }
        break;
    }
}

static std::vector<ParseOrderData> ParseTestCases() {
    std::vector<ParseOrderData> r;
    r.push_back(ParseOrderData{
                "{\"type\": \"Limit\", \"order\": {\"direction\": \"Buy\", \"id\":1, \"price\":14, \"quantity\": 20 }}",
                OrderDirection::Buy,
                OrderType::Limit,
                std::make_shared<LimitOrder>(TimePoint(), 1, 14, 20)
                });

    r.push_back(ParseOrderData{
                "{\"type\": \"Limit\", \"order\": {\"direction\": \"Sell\", \"id\":100, \"price\":120, \"quantity\": 42 }}",
                OrderDirection::Sell,
                OrderType::Limit,
                std::make_shared<LimitOrder>(TimePoint(), 100, 120, 42)
                });

    r.push_back(ParseOrderData{
                "{\"type\": \"Iceberg\", \"order\": {\"direction\": \"Buy\", \"id\":42, \"price\":1337, \"quantity\": 1938, \"peak\":7 }}",
                OrderDirection::Buy,
                OrderType::Iceberg,
                std::make_shared<IcebergOrder>(TimePoint(), 42, 1337, 1938, 7)
                });

    r.push_back(ParseOrderData{
                "{\"type\": \"Iceberg\", \"order\": {\"direction\": \"Sell\", \"id\":22, \"price\":23, \"quantity\": 22, \"peak\":23 }}",
                OrderDirection::Sell,
                OrderType::Iceberg,
                std::make_shared<IcebergOrder>(TimePoint(), 22, 23, 22, 23)
                });
    return r;
}

INSTANTIATE_TEST_CASE_P(Test, ParseOrderTest, ::testing::ValuesIn(ParseTestCases()));
TEST_P(ParseOrderTest, ParseTest) {
    Run();
}



} // ::Strats
} // ::Test
