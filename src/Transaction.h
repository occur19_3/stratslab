#pragma once

#include "Order.h"

namespace Strats {

struct Transaction {
    Order::Id buyOrderId;
    Order::Id sellOrderId;
    Order::Price price;
    std::uint32_t quantity;

    JsonTree GetJsonTree() const;
};

} // ::Strats
