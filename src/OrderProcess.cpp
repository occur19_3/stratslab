#include "OrderProcess.h"

#include "utils/JsonableToStream.h"
#include "utils/Logger.h"

#include "OrderBook.h"
#include "OrderParser.h"
#include "Transaction.h"

namespace Strats {

void ProcessOrders(OrderBook &order_book, OrderParser &parser, std::istream &is, std::ostream &os)
{
    std::string line;
    while (std::getline(is, line)) {
        auto order = parser.ParseOrder(line);
        if (!order.second) {
            LOG("skiping" LGS(line));
            continue;
        }
        auto transactions = order_book.AddOrder(order.first, std::move(order.second));
        os << order_book;
        for (const auto &t: transactions) os << t;
    }
}

} // ::Strats
