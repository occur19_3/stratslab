#include "IcebergOrder.h"

namespace Strats {

IcebergOrder::IcebergOrder(TimePoint creation_time, Id id, Price price, std::uint32_t quantity, std::uint32_t peak):
    Order(creation_time, id, price, quantity), peak(peak)
{}

} // ::Strats
