#pragma once

#include "Order.h"

namespace Strats {

class IcebergOrder: public Order {
public:
    IcebergOrder(TimePoint creation_time, Id id, Price price, std::uint32_t quantity, std::uint32_t peak);

private:
    virtual std::uint32_t GetSingleTransactionQuantity() const { return std::min(quantity, peak); }
    std::uint32_t peak;
};

} // ::Strats
