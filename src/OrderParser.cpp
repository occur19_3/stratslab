#include "OrderParser.h"

#include "utils/Logger.h"

#include "IcebergOrder.h"
#include "LimitOrder.h"

namespace Strats {

OrderParser::OrderParser() {
    RegisterParseFunction(OrderType::Iceberg, &OrderParser::ParseIcebergOrder);
    RegisterParseFunction(OrderType::Limit, &OrderParser::ParseLimitOrder);
}

OrderParser::ParseResult OrderParser::ParseOrder(const std::string &line) const {
    try {
        LOG("will read " << line);
        JsonTree json = ReadJson(line);
        std::string type = json["type"];
        OrderType order_type = from_string<OrderType>(type);
        JsonValue order_json = json["order"];
        return parse_functions.at(order_type)(order_json);
    } catch (std::exception &e) {
        LOG("error: W" << e.what());
    }
    return ParseResult(OrderDirection::Buy, nullptr);
}

void OrderParser::RegisterParseFunction(const OrderType order, ParseFunction parse_f) {
    parse_functions.emplace(order, std::move(parse_f));
}

std::tuple<OrderDirection, Order::Id, Order::Price, std::uint32_t> OrderParser::ParseBaseOrder(const JsonValue &json) {
    std::string direction = json["direction"];
    Order::Id id = json["id"];
    Order::Price price = json["price"];
    std::uint32_t quantity = json["quantity"];
    return std::tuple(
                from_string<OrderDirection>(direction), id, price, quantity
            );
}

OrderParser::ParseResult OrderParser::ParseLimitOrder(const JsonValue &json) {
    auto base_tuple = ParseBaseOrder(json);
    return ParseResult(std::get<OrderDirection>(base_tuple), std::make_unique<LimitOrder>(now(), std::get<1>(base_tuple), std::get<2>(base_tuple), std::get<3>(base_tuple)));
}

OrderParser::ParseResult OrderParser::ParseIcebergOrder(const JsonValue &json) {
    auto base_tuple = ParseBaseOrder(json);
    std::uint32_t peak = json["peak"];
    return ParseResult(std::get<OrderDirection>(base_tuple), std::make_unique<IcebergOrder>(now(), std::get<1>(base_tuple), std::get<2>(base_tuple), std::get<3>(base_tuple), peak));
}

} // ::Strats
