#pragma once

#include <functional>
#include <istream>
#include <memory>
#include <unordered_map>

#include "Order.h"
#include "utils/JsonTree.h"

namespace Strats {

class OrderParser {
public:
    using ParseResult = std::pair<OrderDirection, std::unique_ptr<Order>>;
    using ParseFunction = std::function<ParseResult(const JsonValue &)>;

    // automatically registers functions for parsing Limit and Iceberg Orders
    OrderParser();

    ParseResult ParseOrder(const std::string &line) const;
    void RegisterParseFunction(const OrderType order, ParseFunction parse_f);

private:
    static std::tuple<OrderDirection, Order::Id, Order::Price, std::uint32_t> ParseBaseOrder(const JsonValue &json);
    static ParseResult ParseLimitOrder(const JsonValue &json);
    static ParseResult ParseIcebergOrder(const JsonValue &json);

    std::unordered_map<OrderType, ParseFunction> parse_functions;
};


} // ::Strats
